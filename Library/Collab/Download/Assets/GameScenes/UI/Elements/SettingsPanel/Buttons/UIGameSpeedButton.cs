﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameSpeedButton : UIPlayerInfoButton
{
    [SerializeField]
    private Sprite speedOne = null, speedTwo = null, speedThree = null;

    protected override void OnButtonClick()
    {
        switch (GameManager.ChangeGameSpeed())
        {
            case GameManager.Speed.Normal:
                myImage.sprite = speedOne;
                break;
            case GameManager.Speed.Fast:
                myImage.sprite = speedTwo;
                break;
            case GameManager.Speed.Fastest:
                myImage.sprite = speedThree;
                break;
        }
    }
}

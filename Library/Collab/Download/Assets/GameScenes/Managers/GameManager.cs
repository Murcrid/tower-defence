﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }
    private static GameManager instance;

    public enum Speed { Normal, Fast, Fastest }
    public enum GameResult { Win, Lose, Quit }

    public delegate void GameEventHandler();
    public static event GameEventHandler GameStartedEvent;
    public static event GameEventHandler GamePausedEvent;
    public static event GameEventHandler GameResumedEvent;

    public delegate void GameEndEventHandler(GameResult result);
    public static event GameEndEventHandler GameEndedEvent;

    public static bool EnemiesExist { get { return ListOfEnemies.Count > 0; } }
    public static bool GameIsActive { get; private set; }
    public static Speed GameSpeed { get; private set; }
    public static List<GameObject> ListOfEnemies;

    public static Speed ChangeGameSpeed(Speed speedToChangeTo)
    {
        GameSpeed = speedToChangeTo;
        switch (GameSpeed)
        {
            case Speed.Normal:
                Time.timeScale = 1.0f;
                break;
            case Speed.Fast:
                Time.timeScale = 3.0f;
                break;
            case Speed.Fastest:
                Time.timeScale = 5.0f;
                break;
        }
        return GameSpeed;
    }
    public static Speed ChangeGameSpeed()
    {
        switch (GameSpeed)
        {
            case Speed.Normal:
                GameSpeed = Speed.Fast;
                break;
            case Speed.Fast:
                GameSpeed = Speed.Fastest;
                break;
            case Speed.Fastest:
                GameSpeed = Speed.Normal;
                break;
        }
        switch (GameSpeed)
        {
            case Speed.Normal:
                Time.timeScale = 1.0f;
                break;
            case Speed.Fast:
                Time.timeScale = 3.0f;
                break;
            case Speed.Fastest:
                Time.timeScale = 5.0f;
                break;
        }
        return GameSpeed;
    }

    public static void StartGame()
    {
        if (!GameIsActive)
        {
            GameIsActive = true;
            instance.InitializeManager();
            ResourceManager.InitializeManager();
            BuildingManager.InitializeManager();
            if (GameStartedEvent != null)
                GameStartedEvent();
        }
    }
    public static void EndGame(GameResult result)
    {
        if (GameIsActive || result == GameResult.Quit)
        {
            GameIsActive = false;
            if (result != GameResult.Quit)
            {
                UIManager.OpenEndGameMenu(result);
                Time.timeScale = 0.5f;
                if (GameEndedEvent != null)
                    GameEndedEvent(result);
            }
            else              
                SceneManager.LoadScene(1);
        }
    }
    public static bool PauseGame()
    {
        if (UIManager.OpenPauseMenu())
        {
            Time.timeScale = 0;
            if (GamePausedEvent != null)
                GamePausedEvent();
            return true;
        }
        return false;
    }
    public static bool ResumeGame()
    {
        switch (GameSpeed)
        {
            case Speed.Normal:
                Time.timeScale = 1;
                break;
            case Speed.Fast:
                Time.timeScale = 3;
                break;
            case Speed.Fastest:
                Time.timeScale = 5;
                break;
        }
        if (GameResumedEvent != null)
            GameResumedEvent();
        return true;
    }

    private void InitializeManager()
    {
        GameSpeed = Speed.Normal;
        ListOfEnemies = new List<GameObject>();
    }
}

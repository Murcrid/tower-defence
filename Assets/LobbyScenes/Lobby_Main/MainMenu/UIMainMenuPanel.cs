﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainMenuPanel : UIPanel
{
    public void NewGameButtonClick()
    {
        UIManager.OpenNewGameWindow();
    }
    public void ExitGameButtonClick()
    {
        GameManager.CloseApplication();
    }
    public void ContinueGameButtonClick()
    {
        UIManager.OpenLoadGameWindow();
    }
}

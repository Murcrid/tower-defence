﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UINewGamePanel : UIPanel
{
    public InputField nameInput;
    public Scrollbar difficultyScrollbar;
    public Toggle difficultyToggle;

    public void CreateProfileButtonClick()
    {
        if (nameInput.text != "")
            GameManager.NewGame(nameInput.text, GetDifficulty(), GetTutorialBool());
    }

    private bool GetTutorialBool()
    {
        return difficultyToggle.isOn;
    }

    private SaveFile.Difficulty GetDifficulty()
    {
        SaveFile.Difficulty returnValue = SaveFile.Difficulty.Easy;
        if (difficultyScrollbar.value < 0.33f)
            returnValue = SaveFile.Difficulty.Easy;
        else if (difficultyScrollbar.value > 0.33f && difficultyScrollbar.value < 0.66f)
            returnValue = SaveFile.Difficulty.Normal;
        else
            returnValue = SaveFile.Difficulty.Hard;
        return returnValue;
    }
    public void BackButtonClick()
    {
        UIManager.OpenMainMenuWindow();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using System;
using System.Runtime.Serialization;

public class UILoadGamePanel : UIPanel
{
    public GameObject loadedFileButtonPrefab;
    public Transform listofLoadableFileButtons;
    public GameObject deleteButton, loadButton;

    private SaveFile selectedFile;
    private GameObject[] loadFileButtons;

    protected override void Start()
    {
        LoadSaveFiles();
        base.Start();
    }
    public void SelectFileButtonClick(SaveFile file)
    {
        foreach (var button in loadFileButtons)
        {
            if (file == button.GetComponent<UILoadFileButton>().myFile)
                button.GetComponent<Image>().color = Color.green;
            else
                button.GetComponent<Image>().color = Color.white;
        }
        selectedFile = file;
        deleteButton.SetActive(true);
        loadButton.SetActive(true);
    }
    public void DeleteFileButtonClick()
    {
        if(GameManager.DeleteGame(selectedFile))
        {
            deleteButton.SetActive(false);
            loadButton.SetActive(false);
            selectedFile = null;
            LoadSaveFiles();
        }
    }
    public void LoadFileButtonClick()
    {
        GameManager.LoadGame(selectedFile);
    }
    public void BackButtonClick()
    {
        UIManager.OpenMainMenuWindow();
    }
    private void LoadSaveFiles()
    {
        DestroyOldFiles();
        CreateLoadableButtons();
    }
    private void DestroyOldFiles()
    {
        int originalAmount = listofLoadableFileButtons.transform.childCount;
        for (int index = 0; index < originalAmount; index++)
        {
            DestroyImmediate(listofLoadableFileButtons.transform.GetChild(0).gameObject);
        }
    }
    private void CreateLoadableButtons()
    {
        int fileIndex = 0;
        loadFileButtons = new GameObject[Directory.GetFiles(Application.persistentDataPath, "*.sf").Length];
        
        foreach (var path in Directory.GetFiles(Application.persistentDataPath, "*.sf"))
        {
            FileStream file = File.Open(path, FileMode.Open);
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                loadFileButtons[fileIndex] = Instantiate(loadedFileButtonPrefab);
                loadFileButtons[fileIndex].transform.SetParent(listofLoadableFileButtons, false);
                loadFileButtons[fileIndex++].GetComponent<UILoadFileButton>().Initialize((SaveFile)bf.Deserialize(file));
            }
            catch (SerializationException e)
            {
                Debug.Log("Error in serialization: " + e.Message);
                throw;
            }
            finally
            {
                file.Close();
            }                               
        }
    }
}

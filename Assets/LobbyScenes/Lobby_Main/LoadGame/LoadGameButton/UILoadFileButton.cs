﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using System;

public class UILoadFileButton : Button
{
    internal SaveFile myFile;

    protected void OnLoadButtonClick()
    {
        GetComponentInParent<UILoadGamePanel>().SelectFileButtonClick(myFile);
    }
    public void Initialize(SaveFile fileName)
    {
        myFile = fileName;
        onClick.AddListener(OnLoadButtonClick);
        GetComponentInChildren<Text>().text = myFile.PlayerName;
    }
}

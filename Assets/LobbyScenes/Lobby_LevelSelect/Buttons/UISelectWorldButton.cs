﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UISelectWorldButton : Button
{
    private Image myImage;
    [HideInInspector]
    public World myWorld;
    private Text myName;
    private UILevelSelectPanel mySender;

    public void Initialize(World world, UILevelSelectPanel sender)
    {
        myName.text = world.Name;
        myWorld = world;
        mySender = sender;
        onClick.AddListener(OnButtonClick);
    }
    protected void OnButtonClick()
    {
        mySender.LoadWorldButtonClick(myWorld);
    }
    protected override void Awake()
    {
        myImage = gameObject.GetComponent<Image>();
        myName = GetComponentInChildren<Text>();
        base.Awake();
    }
}


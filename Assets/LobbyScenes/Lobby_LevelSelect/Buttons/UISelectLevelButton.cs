﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UISelectLevelButton : Button
{
    private Image myImage;
    private Level myLevel;
    private Text myName;

    public void Initialize(Level level)
    {
        myName.text = level.Name;
        myLevel = level;
        if (myLevel.levelData.IsUnlocked)
        {
            onClick.AddListener(OnButtonClick);
            myImage.sprite = level.availableSprite;
            myName.color = Color.green;
        }
        else
        {
            myImage.sprite = level.unavailableSprite;
            myName.color = Color.red;
        }     
    }
    protected void OnButtonClick()
    {
        if (myLevel.levelData.IsUnlocked)
        {
            GameManager.SetAndLoadLevel(myLevel);
        }
    }
    protected override void Awake()
    {
        myImage = gameObject.GetComponent<Image>();
        myName = GetComponentInChildren<Text>();
        base.Awake();
    }
}

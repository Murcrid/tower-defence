﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class UILevelSelectPanel : UIPanel
{
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }
    private static UILevelSelectPanel instance;

    public Sprite selectedButtonSprite, deselectedButtonSprite;
    public Transform levelButtonList = null, worldButtonList = null;
    public GameObject levelButtonPrefab = null, worldButtonPrefab = null;
    private List<GameObject> listOfInstansiatedLevelButtons = new List<GameObject>();
    private List<GameObject> listOfInstansiatedWorldButtons = new List<GameObject>();
    public enum Levels { Forest, Town }

    protected override void Start()
    {
        InitializeWorldButtons();
        InitializeLevelButtons();
        base.Start();
    }
    private void InitializeWorldButtons()
    {
        if (GameManager.LoadedWorld == null)
            GameManager.SetLoadedWorld(GameManager.Worlds[1]);
        while (instance.listOfInstansiatedWorldButtons.Count > 0)
        {
            Destroy(instance.listOfInstansiatedWorldButtons[0]);
            instance.listOfInstansiatedWorldButtons.Remove(instance.listOfInstansiatedWorldButtons[0]);
        }
        foreach (var world in GameManager.Worlds)
        {
            GameObject button = Instantiate(instance.worldButtonPrefab);
            button.GetComponent<UISelectWorldButton>().Initialize(world, this);
            button.transform.SetParent(instance.worldButtonList, false);
            instance.listOfInstansiatedWorldButtons.Add(button);
        }
    }
    private void InitializeLevelButtons()   
    {
        while (instance.listOfInstansiatedLevelButtons.Count > 0)
        {
            Destroy(instance.listOfInstansiatedLevelButtons[0]);
            instance.listOfInstansiatedLevelButtons.Remove(instance.listOfInstansiatedLevelButtons[0]);
        }
        foreach (var level in GameManager.LoadedWorld.Levels)
        {
            GameObject button = Instantiate(instance.levelButtonPrefab);
            button.GetComponent<UISelectLevelButton>().Initialize(level);
            button.transform.SetParent(instance.levelButtonList, false);
            instance.listOfInstansiatedLevelButtons.Add(button);
        }
    }
    public void LoadWorldButtonClick(World world)
    {
        GameManager.SetLoadedWorld(world);
        InitializeWorldButtons();
        InitializeLevelButtons();
    }
    public void WatchAddButtonClick()
    {
        AdManager.PlayAdd();
    }
    public void BackButtonClick()
    {
        UIManager.OpenMainMenuWindow();
    }
}

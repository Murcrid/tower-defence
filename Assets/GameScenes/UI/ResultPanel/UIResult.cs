﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResult : MonoBehaviour
{
    public Text resultTextField;

    private void Start()
    {
        transform.SetParent(GameObject.Find("UIResultWindowSpot").transform, false);
    }

    public void Initialize(GameResult result)
    {
        switch (result.result)
        {
            case GameResult.Result.Win:
                resultTextField.text = "Victory!";
                resultTextField.color = Color.green;
                break;
            case GameResult.Result.Lose:
                resultTextField.text = "Defeat";
                resultTextField.color = Color.red;
                break;
            case GameResult.Result.Quit:
                resultTextField.text = "Quit";
                resultTextField.color = Color.red;
                break;
        }
    }
    public void BackToMenuButtonClick()
    {
        GameManager.EndGame(GameResult.Result.Quit);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStatusBar : StatusBar
{
    [SerializeField]
    private RectTransform healthBarForeGround;

    public void ChangeHealthBar(float maxHp, float curHp)
    {
        if (healthBarForeGround != null)
            healthBarForeGround.sizeDelta = new Vector2(curHp / maxHp * 100, 1);
        else
            Debug.Log("Trying to change healthbar in towerObject");
    }
}

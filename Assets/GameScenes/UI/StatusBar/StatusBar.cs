﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class StatusBar: MonoBehaviour
{
    [SerializeField]
    protected GameObject statusImagePrefab;
    [SerializeField]
    protected Transform statusImageList;

    protected void Update ()
    {
        transform.LookAt(2 * transform.position - Camera.main.transform.position);
	}

    public void AddNewStatusIcon(Sprite image, float duration)
    {
        if (statusImageList != null)
        {
            GameObject statusImage = Instantiate(statusImagePrefab);
            statusImage.transform.SetParent(statusImageList, false);
            statusImage.GetComponent<Image>().sprite = image;
            Destroy(statusImage, duration);
        }
    }
}

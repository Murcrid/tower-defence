﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPauseMenu : MonoBehaviour
{
    private void Start()
    {
        transform.SetParent(GameObject.Find("UIPauseMenuSpot").transform, false);
    }
    public void ExitGameButtonClick()
    {
        GameManager.EndGame(GameResult.Result.Quit);
    }
    public void ResumeGameButtonClick()
    {       
        GameManager.ResumeGame();
        Destroy(gameObject);
    }
}

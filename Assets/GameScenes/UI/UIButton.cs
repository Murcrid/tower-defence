﻿using UnityEngine.UI;

[System.Serializable]
public abstract class UIButton : Selectable
{
    protected Image myImage;

    protected override void Awake()
    {
        myImage = gameObject.GetComponent<Image>();
        base.Awake();
    }
}

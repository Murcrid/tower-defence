﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameMain : UIPanel
{
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }
    private static UIGameMain instance;

    public GameObject  playerInfoPrefab = null, specialityMenuPrefab = null, buildingMenuPrefab = null, towerMenuPrefab = null;

    private bool infoPanelIsOpen;
    private GameObject activeMenuWindow, activeInfoWindow;
    private Tower myTower;

    public static void RefreshTowerMenu()
    {
        if (instance.myTower != null && instance.activeInfoWindow != null)
            OpenTowerMenu(instance.myTower);
    }
    public static GameObject OpenTowerMenu(Tower tower)
    {
        if (instance.activeInfoWindow != null)
            Destroy(instance.activeInfoWindow);
        instance.activeInfoWindow = Instantiate(instance.towerMenuPrefab);
        instance.activeInfoWindow.transform.SetParent(instance.transform, false);
        instance.activeInfoWindow.GetComponent<UITowerMenu>().InitializeMenu(tower);
        instance.myTower = tower;
        return instance.activeInfoWindow;
    }
    public static bool TogglePlayerInfo()
    {
        instance.infoPanelIsOpen = !instance.infoPanelIsOpen;
        if (instance.activeInfoWindow != null)
            Destroy(instance.activeInfoWindow);
        if (instance.infoPanelIsOpen)
        {
            instance.activeInfoWindow = Instantiate(instance.playerInfoPrefab);
            instance.activeInfoWindow.transform.SetParent(instance.transform, false);
        }        
        return instance.infoPanelIsOpen;
    }

    public static GameObject OpenTowersMenu()
    {
        if (instance.activeMenuWindow != null)
            Destroy(instance.activeMenuWindow);
        instance.activeMenuWindow = Instantiate(instance.buildingMenuPrefab);
        instance.activeMenuWindow.transform.SetParent(instance.transform, false);
        return instance.activeMenuWindow;
    }
    public static GameObject OpenSpecialsMenu()
    {
        if (instance.activeMenuWindow != null)
            Destroy(instance.activeMenuWindow);
        instance.activeMenuWindow = Instantiate(instance.specialityMenuPrefab);
        instance.activeMenuWindow.transform.SetParent(instance.transform, false);
        return instance.activeMenuWindow;
    }
}

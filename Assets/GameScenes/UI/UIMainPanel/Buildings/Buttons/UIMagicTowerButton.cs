﻿using UnityEngine.EventSystems;

public class UIMagicTowerButton : UIButton
{
    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!GameManager.GameIsPaused)
        {
            BuildingManager.StartBuilding(3);
            base.OnPointerDown(eventData);
        }
    }
}

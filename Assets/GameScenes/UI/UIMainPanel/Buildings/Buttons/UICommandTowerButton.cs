﻿using UnityEngine.EventSystems;

public class UICommandTowerButton : UIButton
{
    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!GameManager.GameIsPaused)
        {
            BuildingManager.StartBuilding(2);
            base.OnPointerDown(eventData);
        }
    }
}

﻿using UnityEngine.EventSystems;

public class UICannonTowerButton : UIButton
{
    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!GameManager.GameIsPaused)
        {
            BuildingManager.StartBuilding(1);
            base.OnPointerDown(eventData);
        }
    }
}

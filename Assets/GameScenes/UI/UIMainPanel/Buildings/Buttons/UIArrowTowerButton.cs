﻿using UnityEngine.EventSystems;
using UnityEngine;

public class UIArrowTowerButton : UIButton
{
    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!GameManager.GameIsPaused)
        {
            BuildingManager.StartBuilding(0);
            base.OnPointerDown(eventData);
        }
    }
}

﻿using UnityEngine.EventSystems;

public class UISlowTowerButton : UIButton
{
    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!GameManager.GameIsPaused)
        {
            BuildingManager.StartBuilding(4);
            base.OnPointerDown(eventData);
        }
    }
}

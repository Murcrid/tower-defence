﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBuildings : MonoBehaviour
{
    public Image arrowTowerImage;
    public Sprite arrowTowerSprite, arrowTowerErrorSprite;
    public Image cannonTowerImage;
    public Sprite cannonTowerSprite, cannonTowerErrorSprite;
    public Image commandTowerImage;
    public Sprite commandTowerSprite, commandTowerErrorSprite;
    public Image magicTowerImage;
    public Sprite magicTowerSprite, magicTowerErrorSprite;
    public Image slowTowerImage;
    public Sprite slowTowerSprite, slowTowerErrorSprite;

    protected void OnDestroy()
    {
        ResourceManager.OnGoldChanged -= ResourceManager_OnGoldChanged;
    }

    private void Start()
    {
        ResourceManager.OnGoldChanged += ResourceManager_OnGoldChanged;
        ResourceManager_OnGoldChanged(ResourceManager.PlayerGold);
    }

    private void ResourceManager_OnGoldChanged(float newValue)
    {
        if (newValue >= BuildingManager.Buildings[0].GetComponent<Tower>().Cost)
            arrowTowerImage.sprite = arrowTowerSprite;
        else
            arrowTowerImage.sprite = arrowTowerErrorSprite;

        if (newValue >= BuildingManager.Buildings[1].GetComponent<Tower>().Cost)
            cannonTowerImage.sprite = cannonTowerSprite;
        else
            cannonTowerImage.sprite = cannonTowerErrorSprite;

        if (newValue >= BuildingManager.Buildings[2].GetComponent<Tower>().Cost)
            commandTowerImage.sprite = commandTowerSprite;
        else
            commandTowerImage.sprite = commandTowerErrorSprite;

        if (newValue >= BuildingManager.Buildings[3].GetComponent<Tower>().Cost)
            magicTowerImage.sprite = magicTowerSprite;
        else
            magicTowerImage.sprite = magicTowerErrorSprite;

        if (newValue >= BuildingManager.Buildings[4].GetComponent<Tower>().Cost)
            slowTowerImage.sprite = slowTowerSprite;
        else
            slowTowerImage.sprite = slowTowerErrorSprite;
    }
}

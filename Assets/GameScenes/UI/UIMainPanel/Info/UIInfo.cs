﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInfo : MonoBehaviour
{
    public Text playerNameField = null, gameDifficultyField = null;

    private void Start()
    {
        playerNameField.text = GameManager.PlayerName;
        gameDifficultyField.text = GameManager.GameDifficulty.ToString();
    }
}

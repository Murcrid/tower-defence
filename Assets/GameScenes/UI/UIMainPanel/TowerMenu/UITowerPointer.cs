﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class UITowerPointer : MonoBehaviour
{
    private LineRenderer lineRenderer;
    float myRadius;

    public void Initialize(float radius, Transform targetTransform)
    {
        myRadius = radius;
        lineRenderer = GetComponent<LineRenderer>();
        transform.position = targetTransform.position + new Vector3(0, 25, 0);
        transform.SetParent(targetTransform);
        DrawRadiusCircle();
    }

    private void DrawRadiusCircle()
    {
        float x;
        float z;

        float angle = 20f;
        int segments = Mathf.RoundToInt(myRadius);

        lineRenderer.positionCount = segments + 1;

        for (int i = 0; i <= segments; i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * myRadius;
            z = Mathf.Cos(Mathf.Deg2Rad * angle) * myRadius;

            lineRenderer.SetPosition(i, new Vector3(x, -24.5f, z));

            angle += (360f / segments);
        }
    }
}

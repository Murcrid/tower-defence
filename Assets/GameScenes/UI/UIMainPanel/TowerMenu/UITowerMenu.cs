﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITowerMenu : MonoBehaviour
{
    public GameObject towerPointerPrefab = null;
    public Text typeField = null, tierField = null;
    public Image attackStyleImage;
    public Sprite attackStyleLastSprite = null, attackStyleNearSprite = null, attackStyleFirstSprite = null;
    public Image upgradeImage;
    public Sprite upgradeSprite = null, upgradeErrorSprite = null;
    public Image specialityImage;

    private GameObject myTowerPointer;
    private Tower myTower;

    private void OnDestroy()
    {
        Tower.OnTowerDestroyed -= Tower_OnTowerDestroyed;
        ResourceManager.OnGoldChanged -= ResourceManager_OnGoldChanged;
        if (myTowerPointer != null)
            Destroy(myTowerPointer);
    }
    public void InitializeMenu(Tower tower)
    {
        myTower = tower;
        Tower.OnTowerDestroyed += Tower_OnTowerDestroyed;
        ResourceManager.OnGoldChanged += ResourceManager_OnGoldChanged;
        ResourceManager_OnGoldChanged(ResourceManager.PlayerGold);
        myTowerPointer = Instantiate(towerPointerPrefab);
        myTowerPointer.GetComponent<UITowerPointer>().Initialize(myTower.AttackRange, myTower.transform);
        SetElements();
    }
    private void SetElements()
    {
        typeField.text = myTower.Type.ToString().Insert(myTower.Type.ToString().Length - 5, " ");
        tierField.text = myTower.Tier.ToString().Insert(4, " ");

        if (myTower.GetComponent<Tower_Cannon>() || myTower.GetComponent<Tower_Magic>() || myTower.GetComponent<Tower_Command>())
            Destroy(attackStyleImage.gameObject);

        if (myTower.UpgradePrefab == null)
            Destroy(upgradeImage.gameObject);

        specialityImage.sprite = myTower.SpecialitySprite;

        if (!myTower.HasSpecialAbility || myTower.SpecialAbilityIsUsed)
            Destroy(specialityImage.gameObject);
    }

    public void UpgradeButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            myTower.UpgradeTower();
        }
    }
    public void SellButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            myTower.SellTower();
        }
    }
    public void AttackModeButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            switch (myTower.RotateAttackMode())
            {
                case Tower.TowerAttackMode.First:
                    attackStyleImage.sprite = attackStyleFirstSprite;
                    break;
                case Tower.TowerAttackMode.Last:
                    attackStyleImage.sprite = attackStyleLastSprite;
                    break;
                case Tower.TowerAttackMode.Near:
                    attackStyleImage.sprite = attackStyleNearSprite;
                    break;
            }
            Debug.Log(myTower.AttackMode);
        }
    }
    public void SpecialityButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            myTower.UseSpecialAbility();
        }
    }
    public void CloseTowerMenuButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            Destroy(GetComponentInParent<UITowerMenu>().gameObject);
        }
    }

    private void ResourceManager_OnGoldChanged(float newValue)
    {
        if (myTower.UpgradePrefab != null && newValue >= myTower.UpgradePrefab.GetComponent<Tower>().Cost)
            upgradeImage.sprite = upgradeSprite;
        else
            upgradeImage.sprite = upgradeErrorSprite;
    }
    private void Tower_OnTowerDestroyed(Tower sender)
    {
        if (myTower == sender)
            Destroy(gameObject);
    }
}


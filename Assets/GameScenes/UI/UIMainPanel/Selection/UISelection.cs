﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelection : MonoBehaviour
{
    public GameObject towersButton, specialsButton;

    private GameObject currentlyActiveButton;

    private void Start()
    {
        TowersButtonClick();
    }

    public void TowersButtonClick()
    {
        UIGameMain.OpenTowersMenu();
        if (currentlyActiveButton != towersButton)
        {
            currentlyActiveButton = towersButton;
            towersButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(30, -10, 0);
            specialsButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(-30, 0, 0);
        }
    }
    public void SpecialsButtonClick()
    {
        UIGameMain.OpenSpecialsMenu();
        if (currentlyActiveButton != specialsButton)
        {
            currentlyActiveButton = specialsButton;
            towersButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(30, 0, 0);
            specialsButton.GetComponent<RectTransform>().anchoredPosition = new Vector3(-30, -10, 0);
        }
    }
}

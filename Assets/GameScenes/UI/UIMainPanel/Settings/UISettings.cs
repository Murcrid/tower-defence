﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISettings : MonoBehaviour
{
    public Image speedImage;
    public Sprite speedOne = null, speedTwo = null, speedThree = null;
    public Image infoImage;
    public Sprite playerInfoOpen = null, playerInfoClosed = null;
    public Image lockImage;
    public Text waveText;
    public Sprite lockedImage = null, unlockedImage = null;
    

    public void SpawningLockButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            if (Spawner.ToggleConstantSpawning())
            {
                lockImage.sprite = lockedImage;
                waveText.text = "Repeated Spawning";
            }
            else
            {
                lockImage.sprite = unlockedImage;
                waveText.text = "Click for next wave";
            }
        }
    }
    public void NextWaveButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            Spawner.NextWave();
        }
    }
    public void GameSpeedButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            switch (GameManager.RotateGameSpeed())
            {
                case GameManager.Speed.Normal:
                    speedImage.sprite = speedOne;
                    break;
                case GameManager.Speed.Fast:
                    speedImage.sprite = speedTwo;
                    break;
                case GameManager.Speed.Fastest:
                    speedImage.sprite = speedThree;
                    break;
            }
        }
    }
    public void PlayerInfoButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            if (UIGameMain.TogglePlayerInfo())
                infoImage.sprite = playerInfoOpen;
            else
                infoImage.sprite = playerInfoClosed;
        }
    }
    public void OptionsButtonClick()
    {
        if (!GameManager.GameIsPaused)
        {
            GameManager.PauseGame();
        }
    }
}

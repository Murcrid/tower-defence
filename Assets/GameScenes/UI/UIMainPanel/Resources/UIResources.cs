﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResources : MonoBehaviour
{
    [SerializeField]
    private Text goldValueText = null, livesValueText = null, scoreValueText = null, powderValueText = null;

    protected void Awake()
    {
        ResourceManager.OnGoldChanged += OnGoldChange;
        ResourceManager.OnLivesChanged += OnLivesChange;
        ResourceManager.OnScoreChanged += OnScoreChange;
        ResourceManager.OnPowderChanged += OnPowderChange;
    }
    protected void OnDestroy()
    {
        ResourceManager.OnGoldChanged -= OnGoldChange;
        ResourceManager.OnLivesChanged -= OnLivesChange;
        ResourceManager.OnScoreChanged -= OnScoreChange;
        ResourceManager.OnPowderChanged -= OnPowderChange;
    }
    private void OnPowderChange(float newValue)
    {
        powderValueText.text = newValue.ToString();
    }
    private void OnGoldChange(float newValue)
    {
        goldValueText.text = newValue.ToString();
    }
    private void OnLivesChange(float newValue)
    {
        livesValueText.text = newValue.ToString();
    }
    private void OnScoreChange(float newValue)
    {
        scoreValueText.text = newValue.ToString();
    }
}

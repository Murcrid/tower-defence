﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIEventMessage : Selectable {

    public Text messageTextField;
    public Image imageSpriteField;

    public enum EventMessageType { NoGold, WaveStarted, WaveCleared }
    [SerializeField]
    private Sprite noGoldImage = null, waveStartedImage = null, waveClearedImage = null;

    private void GeneralInitialization()
    {
        transform.SetParent(GameObject.Find("UIEventSpot").transform, false);
        Destroy(gameObject, 2);
    }
    public override void OnPointerDown(PointerEventData eventData)
    {
        Destroy(gameObject);
        base.OnPointerDown(eventData);
    }
    public void InitializeCustomEventMessage(string text, Color textColor, Sprite image)
    {
        GeneralInitialization();
        messageTextField.text = text;
        messageTextField.color = textColor;
        imageSpriteField.sprite = image;
    }
    public void InitializePresetEventMessage(EventMessageType messageType)
    {
        GeneralInitialization();
        switch (messageType)
        {
            case EventMessageType.NoGold:
                messageTextField.text = "Not Enough Gold, Sire..";
                messageTextField.color = Color.red;
                imageSpriteField.sprite = noGoldImage;
                break;
            case EventMessageType.WaveStarted:
                messageTextField.text = "Wave " + Spawner.WaveIndex + " Started!";
                messageTextField.color = Color.yellow;
                imageSpriteField.sprite = waveStartedImage;
                break;
            case EventMessageType.WaveCleared:
                messageTextField.text = "Wave " + Spawner.WaveIndex + " Cleared!";
                messageTextField.color = Color.green;
                imageSpriteField.sprite = waveClearedImage;
                break;
        }
    }
    public void CloseButtonClick()
    {
        Destroy(gameObject);
    }
}

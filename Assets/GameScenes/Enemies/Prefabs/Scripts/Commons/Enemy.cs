﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public abstract class Enemy : MonoBehaviour {

    private bool _IsDying, _IsSlowed;
    private int _TravelIndex;
    private float _Health;

    [SerializeField]
    private int _GoldValue = 10,  _Damage = 1, _ScoreValue = 1;
    [SerializeField]
    private float _MaxHealth = 2, _MovementSpeed = 5;
    protected float currentSpecialCooldown;

    protected Animator animator;
    protected Transform targetPathNode;
    protected GameObject path;

    public delegate void EnemyEventHandler(Enemy sender);
    public static event EnemyEventHandler OnEnemyDeath;
    public static event EnemyEventHandler OnReachedGoal;

    public enum Type { Basic, LightCavalry, HeavyCavalry, LightInfantry, HeavyInfantry, Shaman, MountedShaman, Spearman, Archer }

    public abstract Type EnemyType { get; }
    public abstract float SpecialCooldown { get; }
    public float MovementSpeed {
        get
        {
            return AdjustForDifficulty(_MovementSpeed, false);
        }
        internal set
        {
            _MovementSpeed = value;
            if (_MovementSpeed <= 2 && animator != null)
                animator.SetFloat("AnimationOffset", _MovementSpeed / 2);
            else if (_MovementSpeed != 0 && animator != null)
                animator.SetFloat("AnimationOffset", 1 + (_MovementSpeed / 50));
            else if (animator != null)
                animator.SetFloat("AnimationOffset", 1);
            if (animator != null)
                animator.SetFloat("Speed", _MovementSpeed);
        }
    }   
    public float Health {
        get { return _Health; }
        internal set
        {
            if (!IsDying)
            {
                _Health = value;                
                if (_Health <= 0)
                    Death();
                else
                    GetComponentInChildren<EnemyStatusBar>().ChangeHealthBar(MaxHealth, _Health);
            }
        }
    }
    public int ScoreValue { get { return _ScoreValue; } protected set { _ScoreValue = value; } }
    public float MaxHealth { get { return _MaxHealth; } protected set { _MaxHealth = value; } }   
    public int GoldValue { get { return _GoldValue; } internal set { _GoldValue = value; } }
    public int TravelIndex { get { return _TravelIndex; } protected set { _TravelIndex = value; } }    
    public int Damage { get { return _Damage; } internal set { _Damage = value; } }
    public bool IsDying { get { return _IsDying; } protected set { _IsDying = value; } }
    public bool IsSlowed { get { return _IsSlowed; } internal set { _IsSlowed = value; } }

    public void AddNewStatusIcon(Sprite image, float duration)
    {
        GetComponentInChildren<StatusBar>().AddNewStatusIcon(image, duration);
    }

    protected virtual void OnDestroy()
    {
        if (GameManager.ListOfEnemies.Contains(gameObject))
            GameManager.ListOfEnemies.Remove(gameObject);
    }
    protected virtual void Start()
    {
        if (!GameManager.ListOfEnemies.Contains(gameObject))
            GameManager.ListOfEnemies.Add(gameObject);
        animator = GetComponent<Animator>();
        transform.SetParent(GameObject.FindGameObjectWithTag("Enemies").transform);
        path = GameObject.FindGameObjectWithTag("Path");
        Health = MaxHealth;
        currentSpecialCooldown = SpecialCooldown;
        MovementSpeed = _MovementSpeed;
        StartCoroutine(Move());
    }

    protected abstract void Special();
    protected virtual void ReachedGoal()
    {
        if (OnReachedGoal != null)
            OnReachedGoal(this);        
        Destroy(gameObject);
    }
    protected virtual void Death()
    {
        IsDying = true;
        if (OnEnemyDeath != null)
            OnEnemyDeath(this);
        animator.SetTrigger("Death");
        Destroy(GetComponentInChildren<StatusBar>().gameObject);
        Destroy(GetComponent<Collider>());
        Destroy(gameObject, 2.5f);       
    }
    protected virtual IEnumerator Move()
    {
        while (TravelIndex < path.transform.childCount)
        {
            if (!IsDying)
            {
                if (targetPathNode == null || Vector3.Distance(targetPathNode.transform.position, transform.position) <= 0.4f * Time.timeScale)
                    targetPathNode = path.transform.GetChild(TravelIndex++);
                if (targetPathNode != null)
                {
                    transform.Translate((targetPathNode.transform.position - transform.position).normalized * Time.deltaTime * MovementSpeed, Space.World);
                    Quaternion targetRotation = Quaternion.LookRotation(targetPathNode.transform.position - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * (1 + MovementSpeed * 0.2f));
                }
                currentSpecialCooldown -= Time.deltaTime;
                if (currentSpecialCooldown <= 0)
                {
                    Special();
                    currentSpecialCooldown = SpecialCooldown;
                }
            }
            yield return new WaitForSeconds(0);
        }
        ReachedGoal();
    }
    protected float AdjustForDifficulty(float value, bool higherIsBetter)
    {
        switch (GameManager.GameDifficulty)
        {
            case SaveFile.Difficulty.Easy:
                if (higherIsBetter)
                    value *= 1.2f;
                else
                    value *= 0.8f;
                break;
            case SaveFile.Difficulty.Normal:
                break;
            case SaveFile.Difficulty.Hard:
                if (higherIsBetter)
                    value *= 0.8f;
                else
                    value *= 1.2f;
                break;
        }
        return value;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Light_Infantry : Enemy
{
    [SerializeField]
    private float _SpecialCooldown;
    public override float SpecialCooldown { get { return _SpecialCooldown; } }

    private Type _EnemyType = Type.LightInfantry;
    public override Type EnemyType { get { return _EnemyType; } }

    protected override void Special()
    {

    }
}

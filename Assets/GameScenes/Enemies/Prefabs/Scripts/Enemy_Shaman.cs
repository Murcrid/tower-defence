﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Shaman : Enemy
{
    [SerializeField]
    private float _SpecialCooldown = 15, _CastRange = 75, _HealPotency = 2;
    public override float SpecialCooldown { get { return _SpecialCooldown; } }

    private Type _EnemyType = Type.Shaman;
    public override Type EnemyType { get { return _EnemyType; } }

    protected override void Special()
    {
        if (currentSpecialCooldown > 0)
            return;
        Enemy currentTarget = null;
        float distToNewTarget = 0;
        foreach (var enemy in GameManager.ListOfEnemies)
        {
            distToNewTarget = Vector3.Distance(transform.position, enemy.transform.position);
            if (distToNewTarget <= _CastRange)
            {
                if (currentTarget == null)
                {
                    if (enemy.GetComponent<Enemy>() && enemy.GetComponent<Enemy>().Health < enemy.GetComponent<Enemy>().MaxHealth)
                    {
                        currentTarget = enemy.GetComponent<Enemy>();
                        break;
                    }
                }
            }
        }
        if (currentTarget != null)
        {
            if (currentTarget.Health <= currentTarget.MaxHealth - _HealPotency)
                currentTarget.Health += _HealPotency;
            else
                currentTarget.Health = currentTarget.MaxHealth;
            currentTarget = null;
        }     
    }
}

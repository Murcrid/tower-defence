﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Dummy : Enemy {

    public int FakeTravelIndex;
    public override Type EnemyType { get { throw new NotImplementedException(); } }

    public override float SpecialCooldown { get { throw new NotImplementedException(); } }

    protected override IEnumerator Move()
    {
        TravelIndex = FakeTravelIndex;
        Debug.Log(TravelIndex + " " + gameObject.name);
        yield return new WaitForSeconds(0);
    }

    protected override void Special()
    {
        throw new NotImplementedException();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "EnemyWave", menuName = "Wave")]
public class Wave : ScriptableObject
{ 
    public int GoldReceivedAfterClear;
    public EnemyGroup[] Groups;
}

[Serializable]
public class EnemyGroup
{
    [SerializeField]
    private Enemy.Type _MainEnemyType;
    [SerializeField]
    private Enemy.Type _SecondaryEnemyType;
    [SerializeField]
    private int _MainSpawnCount = 2, _SecondarySpawnCount = 0;
    [SerializeField]
    private float _IntervalOfSpawns = 0.125f;
    [SerializeField]
    private float _DelayAfterGroup = 5;

    public float IntervalOfSpawns { get { return _IntervalOfSpawns; } }
    public float DelayAfterGroup { get { return _DelayAfterGroup; } }
    public int MainSpawnCount { get { return _MainSpawnCount; } }
    public int SecondarySpawnCount { get { return _SecondarySpawnCount; } }
    public Enemy.Type MainEnemyType { get { return _MainEnemyType; } }
    public GameObject MainEnemyPrefab { get { return Resources.Load<GameObject>("Enemies/Enemy_" + MainEnemyType.ToString()); } }
    public Enemy.Type SecondaryEnemyType { get { return _SecondaryEnemyType; } }
    public GameObject SecondaryEnemyPrefab { get { return Resources.Load<GameObject>("Enemies/Enemy_" + SecondaryEnemyType.ToString()); } }

    public EnemyGroup(Enemy.Type mainType, int amount = 1, Enemy.Type secondaryType = Enemy.Type.Basic, int secondryAmount = 0,  float delay = 1, float delayAfter = 0)
    {    
        _MainEnemyType = mainType;
        _SecondaryEnemyType = secondaryType;
        _SecondarySpawnCount = secondryAmount;     
        _MainSpawnCount = amount;
        _IntervalOfSpawns = delay;
        _DelayAfterGroup = delayAfter;
    }

    internal void AddSpawns(int amount)
    {
        _MainSpawnCount += amount;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Arrow_1 : Tower_Arrow
{
    [SerializeField]
    private GameObject _UpgradePrefab;
    private TowerTier _Tier = TowerTier.TierOne;

    public override TowerTier Tier { get { return _Tier; } }
    public override GameObject UpgradePrefab { get { return _UpgradePrefab; } }

}

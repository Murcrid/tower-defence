﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower_Arrow : Tower
{
    [SerializeField]
    private Sprite _SpecialitySprite;
    public GameObject projectile;
    public int arrowCount;

    private string _Description = "Fires arrows to enemies";
    private string _SpecialityDescription = "Shoots a volley of arrows to all enemies in range";
    private string _SpecialityName = "Volley";
    private TowerType _Type = TowerType.ArrowTower;
    private GameObject[] targets;

    public override Sprite SpecialitySprite { get { return _SpecialitySprite; } }
    public override string SpecialityName { get { return _SpecialityName; } }
    public override string SpecialityDescription { get { return _SpecialityDescription; } }
    public override string Description { get { return _Description; } }
    public override TowerType Type { get { return _Type; } }

    protected override void Start()
    {
        targets = new GameObject[arrowCount];
        base.Start();
    }

    protected override void RotateShootPoint(GameObject rotateTowards)
    {
        if (rotateTowards != null)
        {
            Quaternion targetRotation = Quaternion.LookRotation(rotateTowards.transform.position - transform.GetChild(2).position);
            transform.GetChild(2).rotation = Quaternion.Slerp(transform.GetChild(2).rotation, targetRotation, Time.deltaTime * 5);
        }
    }
    protected override IEnumerator Timer()
    {
        while (true)
        {
            if (GameManager.EnemiesExist)
            {
                attackTimer -= Time.deltaTime;
                RotateShootPoint(targets[0]);
                if (attackTimer <= 0)
                {
                    targets = GetNewTargets(targets);
                    for (int i = 0; i < targets.Length; i++)
                    {
                        if (targets[i] != null)
                        {
                            Shoot(projectile, targets[i]);
                            attackTimer = AttackSpeed;
                        }                          
                    }
                    
                }
            }
            yield return new WaitForSeconds(0);
        }
    }
    protected override void Shoot(GameObject projectile, GameObject target)
    {
        if (projectile != null && target != null)
        {
            GameObject projectileObject = Instantiate(projectile, transform.position + new Vector3(0, 13, 0), transform.GetChild(2).rotation);
            projectileObject.GetComponent<Projectile>().LaunchProjectile(target, AttackPotency);
            projectileObject.transform.SetParent(transform);
        }
    }

    public override void UseSpecialAbility()
    {
        if (!SpecialAbilityIsUsed && HasSpecialAbility && GameManager.EnemiesExist)
        {
            Collider[] colliderTargets = Physics.OverlapSphere(transform.position, AttackRange);
            for (int i = 0; i < colliderTargets.Length; i++)
            {
                if (colliderTargets[i].GetComponent<Enemy>() && !colliderTargets[i].GetComponent<Enemy>().IsDying && Vector3.Distance(transform.position, colliderTargets[i].transform.position) <= AttackRange)
                {
                    Shoot(projectile, colliderTargets[i].gameObject);
                }
            }
            SpecialAbilityIsUsed = true;
            base.UseSpecialAbility();
        }
    }
}

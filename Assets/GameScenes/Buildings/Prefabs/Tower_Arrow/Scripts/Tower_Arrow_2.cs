﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Arrow_2 : Tower_Arrow
{
    [SerializeField]
    private GameObject _UpgradePrefab;
    private TowerTier _Tier = TowerTier.TierTwo;

    public override TowerTier Tier { get { return _Tier; } }
    public override GameObject UpgradePrefab { get { return _UpgradePrefab; } }

}

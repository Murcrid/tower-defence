﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowProjectile : Projectile
{
    protected override void OnTriggerEnter(Collider c)
    {
        if (c.gameObject == target)
        {
            if (c.GetComponent<Enemy>() && !c.GetComponent<Enemy>().IsDying)
            {
                c.GetComponent<Enemy>().Health -= damage;
                Destroy(gameObject);
            }
            else if (c.gameObject.GetComponent<TerrainCollider>())
                Destroy(gameObject);
        }
    }
}

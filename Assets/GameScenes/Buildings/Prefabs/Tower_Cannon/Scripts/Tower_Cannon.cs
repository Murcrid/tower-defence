﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower_Cannon : Tower
{
    [SerializeField]
    private float _AttackRadius;
    [SerializeField]
    private GameObject projectilePrefab = null, targetCirclePrefab = null;
    [SerializeField]
    private Sprite _SpecialitySprite;

    private TowerType _Type = TowerType.CannonTower;
    private GameObject targetCircle;
    private bool targetSet;
    private bool _HasSpecialAbility = true;
    private string _Description = "Shoots explosives to a target area";
    private string _SpecialityDescription = "Reposition the target circle";
    private string _SpecialityName = "Retarget";

    public override Sprite SpecialitySprite { get { return _SpecialitySprite; } }
    public override string SpecialityName { get { return _SpecialityName; } }
    public override string SpecialityDescription { get { return _SpecialityDescription; } }
    public override bool HasSpecialAbility { get { return _HasSpecialAbility; } protected set { _HasSpecialAbility = value; } }
    public override string Description { get { return _Description; } }
    public override float AttackRadius { get { return AdjustForDifficulty(_AttackRadius, true); } protected set { _AttackRadius = value; } }
    public override TowerType Type { get { return _Type; } }
   
    protected override void OnDestroy()
    {
        if (targetCircle != null)
            Destroy(targetCircle);
        base.OnDestroy();
    }

    protected override void RotateShootPoint(GameObject rotateTowards)
    {
        if (rotateTowards != null)
        {
            Quaternion targetRotation = Quaternion.LookRotation(new Vector3(rotateTowards.transform.position.x, transform.GetChild(2).position.y, rotateTowards.transform.position.z) - transform.GetChild(2).position);
            transform.GetChild(2).rotation = Quaternion.Slerp(transform.GetChild(2).rotation, targetRotation, Time.deltaTime * 5);
        }
    }
    protected override IEnumerator Timer()
    {
        while (true)
        {
            if (targetSet && GameManager.EnemiesExist)
            {
                attackTimer -= Time.deltaTime;
                if (targetCircle != null)
                    RotateShootPoint(targetCircle);
                if (attackTimer <= 0)
                {
                    if (targetCircle != null)
                    {
                        Shoot(projectilePrefab, targetCircle);
                        attackTimer = AttackSpeed;
                    }                   
                }
            }
            yield return new WaitForSeconds(0);
        }
    }
    protected override void Shoot(GameObject projectile, GameObject target)
    {
        if (projectile != null && target != null)
        {
            GameObject projectileObject = Instantiate(projectile, transform.GetChild(2).GetChild(0).GetChild(0).position, transform.GetChild(2).GetChild(0).GetChild(0).rotation);
            projectileObject.GetComponent<Projectile>().LaunchProjectileWithRigitBody(target, CalculateTrajectoryVelocity(transform.GetChild(2).GetChild(0).GetChild(0).position, target.transform.position), AttackPotency, AttackRadius);
            projectileObject.transform.SetParent(transform);
        }
    }

    public override void UseSpecialAbility()
    {
        if (!SpecialAbilityIsUsed && HasSpecialAbility && GameManager.EnemiesExist)
        {
            StartCoroutine(SetTarget());
            SpecialAbilityIsUsed = true;
            base.UseSpecialAbility();
        }
        else if(HasSpecialAbility && !GameManager.EnemiesExist)
        {
            StartCoroutine(SetTarget());
            base.UseSpecialAbility();
        }
    }

    private IEnumerator SetTarget()
    {
        RaycastHit hit;
        targetSet = false;
        if (targetCircle == null)
            targetCircle = Instantiate(targetCirclePrefab);
        targetCircle.transform.SetParent(transform);
        do
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit, Mathf.Infinity);
            if (Vector3.Distance(transform.position, hit.point) <= AttackRange)
                targetCircle.transform.position = hit.point;
            else
                targetCircle.transform.position = transform.position + (hit.point - transform.position).normalized * AttackRange;
            yield return new WaitForSeconds(0);
        } while (Input.GetMouseButton(0));
        if (ValidateMousePoint())
            targetSet = true;
        CursorManager.ChangeCursor(CursorManager.CursorType.Default);
    }
    private Vector3 CalculateTrajectoryVelocity(Vector3 origin, Vector3 target)
    {
        float t = Vector3.Distance(origin, target) * 0.1f;
        float vx = (target.x - origin.x) / t;
        float vz = (target.z - origin.z) / t;
        float vy = ((target.y - origin.y) - 0.5f * Physics.gravity.y * t * t) / t;
        return new Vector3(vx, vy, vz);
    }
    private bool ValidateMousePoint()
    {
        if (targetCircle.transform.position.y <= 0.25f)
            return true;
        else
            return false;
    }
}

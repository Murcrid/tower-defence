﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallProjectile : Projectile
{
    protected override void OnTriggerEnter(Collider c)
    {
        if (c.GetComponent<Enemy>() || c is TerrainCollider)
        {
            Collider[] targets = Physics.OverlapSphere(transform.position, radius);
            foreach (var target in targets)
            {
                if (target.GetComponent<Enemy>() && !target.GetComponent<Enemy>().IsDying)
                    target.GetComponent<Enemy>().Health -= damage;
            }
            Destroy(gameObject);
        }
    }
}

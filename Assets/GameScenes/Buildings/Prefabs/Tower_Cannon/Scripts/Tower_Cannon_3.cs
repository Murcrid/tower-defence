﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Cannon_3 : Tower_Cannon
{
    private TowerTier _Tier = TowerTier.TierThree;

    public override TowerTier Tier { get { return _Tier; } }
}

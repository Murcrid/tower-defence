﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Cannon_1 : Tower_Cannon
{
    [SerializeField]
    private GameObject _UpgradePrefab = null;
    private TowerTier _Tier = TowerTier.TierOne;

    public override TowerTier Tier { get { return _Tier; } }
    public override GameObject UpgradePrefab { get { return _UpgradePrefab; } }

}


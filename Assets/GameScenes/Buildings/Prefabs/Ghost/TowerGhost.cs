﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(CapsuleCollider))]
public class TowerGhost : MonoBehaviour
{
    [SerializeField]
    private GameObject towerPointerPrefab;

    private GameObject myTowerPointer;

    public void Initialize(float radius)
    {
        myTowerPointer = Instantiate(towerPointerPrefab);
        myTowerPointer.GetComponent<UITowerPointer>().Initialize(radius, transform);
        myTowerPointer.transform.SetParent(transform);
    }

    private void OnTriggerStay(Collider other)
    {
        if ((!(other.tag == "Tower") && !(other.tag == "PathNode") && !(other.tag == "Blocker")) || BuildingManager.IsColliding == true)
            return;
        BuildingManager.IsColliding = true;
    }

    private void OnTriggerExit(Collider c)
    {
        if (!(c.tag == "Tower") && !(c.tag == "PathNode") && !(c.tag == "Blocker"))
            return;
        BuildingManager.IsColliding = false;
    }
}

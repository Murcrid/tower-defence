﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Command_2 : Tower_Command
{
    [SerializeField]
    private GameObject _UpgradePrefab = null;
    private TowerTier _Tier = TowerTier.TierTwo;

    public override TowerTier Tier { get { return _Tier; } }
    public override GameObject UpgradePrefab { get { return _UpgradePrefab; } }

}

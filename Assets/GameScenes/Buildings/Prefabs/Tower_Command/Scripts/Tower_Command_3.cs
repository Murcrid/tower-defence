﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Command_3 : Tower_Command
{
    private bool _HasSpecialAbility = true;
    private TowerTier _Tier = TowerTier.TierThree;

    public override TowerTier Tier { get { return _Tier; } }
    public override bool HasSpecialAbility { get { return _HasSpecialAbility; }}

}

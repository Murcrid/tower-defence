﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower_Command : Tower
{
    [SerializeField]
    private Sprite boostStatusImage = null;

    private TowerType _Type = TowerType.CommandTower;
    private GameObject targetTower;
    private GameObject target;
    private string _Description = "Buffs other towers to shoot faster";
    private string _SpecialityDescription = "Buffs all towers in range";
    private string _SpecialityName = "Rally";

    public override string Description { get { return _Description; } }
    public override TowerType Type { get { return _Type; } }
    public override Sprite SpecialitySprite { get { return boostStatusImage; } }
    public override string SpecialityName { get { return _SpecialityName; } }
    public override string SpecialityDescription { get { return _SpecialityDescription; } }

    protected override void RotateShootPoint(GameObject rotateTowards)
    {
        throw new NotImplementedException();
    }
    protected override IEnumerator Timer()
    {
        while (true)
        {
            if (GameManager.EnemiesExist || Spawner.WaveIsSpawning)
            {
                attackTimer -= Time.deltaTime;
                if (attackTimer <= 0)
                {
                    target = GetNewNonBoostedTarget();
                    if (target != null)
                    {
                        Shoot(target);
                        attackTimer = AttackSpeed;
                    }
                }
            }
            yield return new WaitForSeconds(0);
        }      
    }
    protected override void Shoot(GameObject target, GameObject fill = null)
    {
        if (target != null)
        {
            StartCoroutine(BoostTimer(target.GetComponent<Tower>(), AttackPotency));
            target.GetComponent<Tower>().AddNewStatusIcon(boostStatusImage, AttackPotency);
        }
    }

    public override void UseSpecialAbility()
    {
        if (!SpecialAbilityIsUsed && HasSpecialAbility && GameManager.EnemiesExist)
        {
            Collider[] colliderTargets = Physics.OverlapSphere(transform.position, AttackRange);
            for (int i = 0; i < colliderTargets.Length; i++)
            {
                if (colliderTargets[i].GetComponent<Tower>() && Vector3.Distance(transform.position, colliderTargets[i].transform.position) <= AttackRange)
                {
                    Shoot(colliderTargets[i].gameObject);
                }
            }
            SpecialAbilityIsUsed = true;
            base.UseSpecialAbility();
        }
    }

    private IEnumerator BoostTimer(Tower tower, float boostDuration)
    {
        if (!tower.IsBoosted)
        {
            tower.IsBoosted = true;
            tower.AttackSpeed /= 1.5f;
            while (boostDuration >= 0)
            {
                boostDuration -= Time.deltaTime;
                yield return new WaitForSeconds(0);
            }
            tower.AttackSpeed *= 1.5f;
            tower.IsBoosted = false;
        }
    }
    private GameObject GetNewNonBoostedTarget(GameObject target = null)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, AttackRange);
        foreach (var collider in colliders)
        {
            GameObject tower = collider.gameObject;
            if (tower != null && tower.GetComponent<Tower>() && !tower.GetComponent<Tower>().IsBoosted && !tower.GetComponent<Tower_Command>())
            {
                target = tower;
                break;
            }
        }
        return target;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower_Slowing : Tower
{
    [SerializeField]
    private Sprite slowStatusImage = null;

    private TowerType _Type = TowerType.SlowingTower;
    private string _Description = "Slows enemies";
    private string _SpecialityDescription = "Slows all enemies in range";
    private string _SpecialityName = "Mass Slow";

    public override Sprite SpecialitySprite { get { return slowStatusImage; } }
    public override string SpecialityName { get { return _SpecialityName; } }
    public override string SpecialityDescription { get { return _SpecialityDescription; } }
    public override string Description { get { return _Description; } }
    public override TowerType Type { get { return _Type; } }

    protected GameObject target;

    protected override void RotateShootPoint(GameObject rotateTowards)
    {
        throw new NotImplementedException();
    }
    protected override IEnumerator Timer()
    {
        while (true)
        {
            if (GameManager.EnemiesExist)
            {
                attackTimer -= Time.deltaTime;
                if (attackTimer <= 0)
                {
                    target = GetNewNonSlowedTarget();
                    if (target != null)
                    {
                        Shoot(target);
                        attackTimer = AttackSpeed;
                    }
                }
            }
            yield return new WaitForSeconds(0);
        }
    }
    protected override void Shoot(GameObject target, GameObject fill = null)
    {
        if (target != null)
        {
            StartCoroutine(SlowTimer(target.GetComponent<Enemy>(), AttackPotency));
            target.GetComponent<Enemy>().AddNewStatusIcon(slowStatusImage, AttackPotency);
        }
    }

    private IEnumerator SlowTimer(Enemy enemy, float slowDuration)
    {
        if (!enemy.IsSlowed)
        {
            enemy.IsSlowed = true;
            enemy.MovementSpeed /= 3;
            while (slowDuration >= 0)
            {
                slowDuration -= Time.deltaTime;
                yield return new WaitForSeconds(0);
            }
            enemy.MovementSpeed *= 3;
            enemy.IsSlowed = false;
        }
    }
    private GameObject GetNewNonSlowedTarget(GameObject target = null)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, AttackRange);
        foreach (var collider in colliders)
        {
            GameObject enemy = collider.gameObject;
            if (enemy != null && enemy.GetComponent<Enemy>() && !enemy.GetComponent<Enemy>().IsDying && !enemy.GetComponent<Enemy>().IsSlowed && Vector3.Distance(transform.position, enemy.transform.position) <= AttackRange)
            {
                if (target == null)
                    target = enemy;
                else
                {
                    switch (AttackMode)
                    {
                        case TowerAttackMode.First:
                            if (target.GetComponent<Enemy>().TravelIndex < enemy.GetComponent<Enemy>().TravelIndex)
                                target = enemy;
                            break;
                        case TowerAttackMode.Last:
                            if (target.GetComponent<Enemy>().TravelIndex > enemy.GetComponent<Enemy>().TravelIndex)
                                target = enemy;
                            break;
                        case TowerAttackMode.Near:
                            if (Vector3.Distance(transform.position, enemy.transform.position) < Vector3.Distance(transform.position, target.transform.position))
                                target = enemy;
                            break;
                    }
                }
            }
        }
        return target;
    }

    public override void UseSpecialAbility()
    {
        if (!SpecialAbilityIsUsed && HasSpecialAbility && GameManager.EnemiesExist)
        {
            Collider[] colliderTargets = Physics.OverlapSphere(transform.position, AttackRange);
            for (int i = 0; i < colliderTargets.Length; i++)
            {
                if (colliderTargets[i].GetComponent<Enemy>() && !colliderTargets[i].GetComponent<Enemy>().IsDying && !colliderTargets[i].GetComponent<Enemy>().IsSlowed && Vector3.Distance(transform.position, colliderTargets[i].transform.position) <= AttackRange)
                {
                    Shoot(colliderTargets[i].gameObject);
                }
            }
            SpecialAbilityIsUsed = true;
            base.UseSpecialAbility();
        }
    }
}

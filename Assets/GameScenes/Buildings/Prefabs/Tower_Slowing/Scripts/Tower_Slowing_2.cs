﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_Slowing_2 : Tower_Slowing
{
    [SerializeField]
    private GameObject _UpgradePrefab;
    private bool _HasSpecialAbility = true;
    private TowerTier _Tier = TowerTier.TierTwo;

    public override TowerTier Tier { get { return _Tier; } }
    public override bool HasSpecialAbility { get { return _HasSpecialAbility; } }
    public override GameObject UpgradePrefab { get { return _UpgradePrefab; } }
}

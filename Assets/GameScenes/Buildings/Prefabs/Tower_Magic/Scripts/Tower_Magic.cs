﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower_Magic : Tower
{
    [SerializeField]
    private Sprite _SpecialitySprite;
    [SerializeField]
    private GameObject effectPrefab;

    private TowerType _Type = TowerType.MagicTower;

    private string _Description = "Summons a cloud of lightning to strike all enemies in range";
    private string _SpecialityDescription = "Immediately summons clouds to strike all enemies in range";
    private string _SpecialityName = "Double Strike";

    public override Sprite SpecialitySprite { get { return _SpecialitySprite; } }
    public override string SpecialityName { get { return _SpecialityName; } }
    public override string SpecialityDescription { get { return _SpecialityDescription; } }
    public override TowerType Type { get { return _Type; } }
    public override string Description { get { return _Description; } }

    protected override void RotateShootPoint(GameObject rotateTowards)
    {
        throw new NotImplementedException();
    }
    protected override void Shoot(GameObject projectile, GameObject target)
    {
        if (target != null)
        {
            GameObject effectObject = Instantiate(projectile, target.transform.position + new Vector3(0, 3, 0), projectile.transform.rotation);
            effectObject.GetComponent<Effect>().Initialize(target, AttackPotency);
            effectObject.transform.SetParent(target.transform);
            Destroy(effectObject, 6);
        }
    }
    protected override IEnumerator Timer()
    {
        while (true)
        {
            if (GameManager.EnemiesExist)
            {
                attackTimer -= Time.deltaTime;
                if (attackTimer <= 0)
                {
                    Collider[] cols = Physics.OverlapSphere(transform.position, AttackRange);
                    foreach (var col in cols)
                    {
                        if (col.GetComponent<Enemy>() && !col.GetComponent<Enemy>().IsDying)
                        {
                            Shoot(effectPrefab, col.gameObject);
                        }
                    }
                    attackTimer = AttackSpeed;
                }
            }
            yield return new WaitForSeconds(0);
        }
    }
    public override void UseSpecialAbility()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, AttackRange);
        foreach (var col in cols)
        {
            if (col.GetComponent<Enemy>() && !col.GetComponent<Enemy>().IsDying)
            {
                Shoot(effectPrefab, col.gameObject);
                SpecialAbilityIsUsed = true;
            }
        }
        base.UseSpecialAbility();
    }
}

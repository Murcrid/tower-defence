﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    private GameObject myTarget;
    private float myPotency;

    public void Initialize(GameObject target, float potency)
    {
        myTarget = target;
        myPotency = potency;
        StartCoroutine(DamageTarget());
    }

    private IEnumerator DamageTarget()
    {
        float timer = 1.8f;
        while (timer >= 0)
        {
            timer -= Time.deltaTime;
            yield return new WaitForSeconds(0);
        }
        if (myTarget.GetComponent<Enemy>() && !myTarget.GetComponent<Enemy>().IsDying)
            myTarget.GetComponent<Enemy>().Health -= myPotency;
    }
}

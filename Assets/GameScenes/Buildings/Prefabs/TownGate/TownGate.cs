﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownGate : MonoBehaviour
{
    [SerializeField]
    private int maxHP = 15;
    private int currentHP;

    private void Start()
    {
        currentHP = maxHP;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Enemy>())
        {
            currentHP -= other.gameObject.GetComponent<Enemy>().Damage;
            Destroy(other.gameObject);
            GetComponentInChildren<EnemyStatusBar>().ChangeHealthBar(maxHP, currentHP);
            if (currentHP <= 0)
                Destroy(gameObject);
        }
    }
}

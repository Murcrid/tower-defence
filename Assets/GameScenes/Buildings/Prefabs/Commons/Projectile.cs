﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    protected GameObject target;
    internal float damage, radius;

    public virtual void LaunchProjectile(GameObject target, float damage, float radius = 0)
    {
        this.target = target;
        this.damage = damage;
        if (radius != 0)
            this.radius = radius;
        StartCoroutine(MoveTowardsTarget());
    }
    public virtual void LaunchProjectileWithRigitBody(GameObject target, Vector3 velocity, float damage, float radius = 0)
    {
        this.target = target;
        this.damage = damage;
        if (radius != 0)
            this.radius = radius;
        if (velocity != default(Vector3))
            GetComponent<Rigidbody>().velocity = velocity;
    }
    protected virtual IEnumerator MoveTowardsTarget()
    {
        Vector3 latestPos = target.transform.position;
        while (target != null)
        {
            latestPos = target.transform.position;
            transform.Translate((target.transform.position + new Vector3(0, target.transform.localScale.y / 2, 0) - transform.position).normalized * Time.deltaTime * 50f, Space.World);
            Quaternion targetRotation = Quaternion.LookRotation(target.transform.position + new Vector3(0, target.transform.localScale.y / 2, 0) - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 5);
            yield return new WaitForSeconds(0);
        }
        if (target == null)
            Destroy(gameObject);
    }
    protected virtual void OnTriggerEnter(Collider c)
    {
        Debug.Log("Unimplemented OnTriggerEnter() on: " + gameObject.name);
    }
}

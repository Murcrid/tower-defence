﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }
    private static BuildingManager instance;

    [SerializeField]
    private GameObject[] _Buildings = null;
    [SerializeField]
    private GameObject _Ghosts = null;
    [SerializeField]
    private Material ghostMaterial = null, errorMaterial = null;    

    public static GameObject[] Buildings { get { return instance._Buildings; } }
    public static GameObject Ghost { get { return instance._Ghosts; } }

    public static bool  IsColliding { get; internal set; }
    public bool BuildingModeActive { get; private set; }

    public static void InitializeManager()
    {
        IsColliding = false;
        instance.BuildingModeActive = false;
    }
    public static bool StartBuilding(int buildingIndex)
    {
        if (ResourceManager.CheckGold(instance._Buildings[buildingIndex].GetComponent<Tower>().Cost))
        {
            GameObject ghostBuilding = Instantiate(Ghost);
            ghostBuilding.GetComponent<TowerGhost>().Initialize(instance._Buildings[buildingIndex].GetComponent<Tower>().AttackRange);
            instance.StartCoroutine(instance.BuildingMode(ghostBuilding, buildingIndex));
            IsColliding = false;
            return true;
        }
        return false;
    }
    private IEnumerator BuildingMode(GameObject ghostBuilding, int buildingIndex)
    {
        RaycastHit hit;
        do
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit, Mathf.Infinity);
            ghostBuilding.transform.position = hit.point;
            if (!IsColliding)
            {
                ghostBuilding.transform.GetComponentInChildren<MeshRenderer>().material = ghostMaterial;
                CursorManager.ChangeCursor(CursorManager.CursorType.Build);
            }
            else
            {
                ghostBuilding.transform.GetComponentInChildren<MeshRenderer>().material = errorMaterial;
                CursorManager.ChangeCursor(CursorManager.CursorType.Error);
            }
            yield return new WaitForSeconds(0);
        } while (Input.GetMouseButton(0));
        ghostBuilding.transform.position = new Vector3(ghostBuilding.transform.position.x, 5, ghostBuilding.transform.position.z);
        if (!IsColliding)
        {
            if (ResourceManager.CheckGold(Buildings[buildingIndex].GetComponent<Tower>().Cost, true))
            {
                Instantiate(Buildings[buildingIndex], ghostBuilding.transform.position, ghostBuilding.transform.rotation);
            }
        }
        Destroy(ghostBuilding);
        CursorManager.ChangeCursor(CursorManager.CursorType.Default);
    }
}

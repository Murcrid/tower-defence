﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Enemy.OnEnemyDeath += Enemy_OnEnemyDeath;
            Enemy.OnReachedGoal += Enemy_OnReachedGoal;
            Tower.OnTowerSold += Tower_OnTowerSold;
            Spawner.OnWaveCleared += SpawnerManager_OnWaveCleared;
        }          
        else if (instance != this)
            Destroy(this);
    }
    private static ResourceManager instance;
    private void OnDestroy()
    {
        Enemy.OnReachedGoal -= Enemy_OnReachedGoal;
        Enemy.OnEnemyDeath -= Enemy_OnEnemyDeath;
        Tower.OnTowerSold -= Tower_OnTowerSold;
        Spawner.OnWaveCleared -= SpawnerManager_OnWaveCleared;
    }
    private static float _PlayerGold;
    private static float _PlayerLives;
    private static float _PlayerScore;
    private static float _PlayerPowder;

    internal static bool AdWatched;

    public delegate void ResourceChangeHandler(float newValue);
    public static event ResourceChangeHandler OnGoldChanged;
    public static event ResourceChangeHandler OnLivesChanged;
    public static event ResourceChangeHandler OnScoreChanged;
    public static event ResourceChangeHandler OnPowderChanged;

    public static float PlayerPowder
    {
        get { return _PlayerPowder; }
        private set
        {
            _PlayerPowder = value;
            if (OnPowderChanged != null)
                OnPowderChanged(_PlayerPowder);
        }
    }
    public static float PlayerGold
    {
        get { return _PlayerGold; }
        private set
        {
            _PlayerGold = value;
            if (OnGoldChanged != null)
                OnGoldChanged(_PlayerGold);
        }
    }
    public static float PlayerLives
    {
        get { return _PlayerLives; }
        private set
        {
            _PlayerLives = value;
            if (OnLivesChanged != null)
                OnLivesChanged(_PlayerLives);
            if (_PlayerLives <= 0)
                GameManager.EndGame(GameResult.Result.Lose);
        }
    }
    public static float PlayerScore
    {
        get { return _PlayerScore; }
        private set
        {
            _PlayerScore = value;
            if (OnScoreChanged != null)
                OnScoreChanged(_PlayerScore);
        }
    }

    public static bool InitializeManager()
    {
        if (AdWatched)
        {
            PlayerGold = GameManager.LoadedLevel.StartingGold * 2;
            PlayerLives = GameManager.LoadedLevel.StartingLives * 2;
            AdWatched = false;
        }
        else
        {
            PlayerGold = GameManager.LoadedLevel.StartingGold;
            PlayerLives = GameManager.LoadedLevel.StartingLives;
        }
        PlayerPowder = 0;
        PlayerScore = 0;
        return true;
    }

    public static bool CheckGold(float value, bool subtract = false)
    {
        if (value <= PlayerGold)
        {
            if (subtract)
                PlayerGold -= value;
            return true;
        }
        UIManager.OpenNewEventMessage(UIEventMessage.EventMessageType.NoGold);
        return false;
    }

    private void Enemy_OnReachedGoal(Enemy sender)
    {
        PlayerLives -= sender.Damage;
    }
    private void SpawnerManager_OnWaveCleared(Wave wave)
    {
        PlayerGold += wave.GoldReceivedAfterClear;
    }
    private void Tower_OnTowerSold(Tower sender)
    {
        PlayerGold += sender.SellPrice;
    }
    private void Enemy_OnEnemyDeath(Enemy enemy)
    {
        PlayerGold += enemy.GoldValue;
    }
}

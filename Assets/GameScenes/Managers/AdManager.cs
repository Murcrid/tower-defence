﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }
    private static AdManager instance;

    public static void PlayAdd()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show("rewardedVideo", new ShowOptions() { resultCallback = HandleAdResult });
        }
        else
            Debug.Log("No add ready");
    }

    private static void HandleAdResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Failed:
                Debug.Log("Failed ad");
                break;
            case ShowResult.Skipped:
                Debug.Log("Skipped ad");
                break;
            case ShowResult.Finished:
                ResourceManager.AdWatched = true;
                break;
        }
    }
}

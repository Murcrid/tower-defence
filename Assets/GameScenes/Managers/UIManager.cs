﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            SceneManager.sceneLoaded += OnSceneLoaded;
            OpenMainMenuWindow();
        }            
        else if (instance != this)
            Destroy(this);
    }
    private static UIManager instance;

    [SerializeField]
    private GameObject pauseMenuPrefab = null, messagePrefab = null, gameResultPrefab = null;
    [SerializeField]
    private GameObject mainMenuWindowPrefab = null, newGameWindowPrefab = null, loadGameWindowPrefab = null, selectLevelWindowPrefab = null, mainGameWindowPrefab = null;

    public delegate void UIEventHandler();
    public static event UIEventHandler OnPauseMenuOpened;
    
    private GameObject activeMainWindow, activeInfoWindow, activeMenuWindow, pauseMenu, endScreen;
    private bool infoPanelIsOpen;

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == 0 && GameManager.LoadedWorld == null)
            OpenMainMenuWindow();
        else if (scene.buildIndex == 0 && GameManager.LoadedWorld != null)
            OpenSelectLevelWindow();
        else
            OpenGameMenuWindow();
    }

    public static GameObject OpenNewEventMessage(UIEventMessage.EventMessageType eventType)
    {
        GameObject eventMessage = Instantiate(instance.messagePrefab);
        eventMessage.GetComponent<UIEventMessage>().InitializePresetEventMessage(eventType);
        return eventMessage;
    }
    public static GameObject OpenEndGameMenu(GameResult result)
    {
        if (instance.endScreen != null)
            Destroy(instance.endScreen);
        instance.endScreen = Instantiate(instance.gameResultPrefab);
        instance.endScreen.GetComponent<UIResult>().Initialize(result);
        return instance.endScreen;
    }
    public static GameObject OpenPauseMenu()
    {
        if (instance.pauseMenu != null)
            Destroy(instance.pauseMenu);
        instance.pauseMenu = Instantiate(instance.pauseMenuPrefab);
        if (OnPauseMenuOpened != null)
            OnPauseMenuOpened();
        return instance.pauseMenu;
    }

    public static void OpenSelectLevelWindow()
    {
        if (instance.activeMainWindow != null)
            Destroy(instance.activeMainWindow);
        instance.activeMainWindow = Instantiate(instance.selectLevelWindowPrefab);
    }
    public static void OpenLoadGameWindow()
    {
        if (instance.activeMainWindow != null)
            Destroy(instance.activeMainWindow);
        instance.activeMainWindow = Instantiate(instance.loadGameWindowPrefab);
    }
    public static void OpenNewGameWindow()
    {
        if (instance.activeMainWindow != null)
            Destroy(instance.activeMainWindow);
        instance.activeMainWindow = Instantiate(instance.newGameWindowPrefab);
    }
    public static void OpenMainMenuWindow()
    {
        if (instance.activeMainWindow != null)
            Destroy(instance.activeMainWindow);
        instance.activeMainWindow = Instantiate(instance.mainMenuWindowPrefab);
    }
    public static void OpenGameMenuWindow()
    {
        if (instance.activeMainWindow != null)
            Destroy(instance.activeMainWindow);
        instance.activeMainWindow = Instantiate(instance.mainGameWindowPrefab);
    }
}


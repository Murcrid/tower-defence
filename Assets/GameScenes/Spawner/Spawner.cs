﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            instance.myLevel = GameManager.LoadedLevel;
            if (instance.myLevel.IsLastLevel)
            {
                int count = 0;
                foreach (var level in GameManager.LoadedWorld.Levels)
                {
                    count += level.levelData.DifferentEscapedEnemies;
                }
                finalEnemies = new EnemyGroup[count];
                int index = 0;
                foreach (var level in GameManager.LoadedWorld.Levels)
                {
                    foreach (var group in level.levelData.EscapedEnemies)
                    {
                        finalEnemies[index++] = new EnemyGroup(group.MainEnemyType, group.MainSpawnCount);
                    }
                }
            }
        }
        else if (instance != this)
            Destroy(this);
    }
    private static Spawner instance;
    private void OnDestroy()
    {
        instance = null;
    }

    private int _WaveIndex = 0;
    private bool constantSpawning;
    private Level myLevel;
    private EnemyGroup[] finalEnemies;

    public delegate void WaveEvent(Wave wave);
    public static event WaveEvent OnWaveSpawnBegun;
    public static event WaveEvent OnWaveSpawningEnded;
    public static event WaveEvent OnWaveCleared;

    public static int WaveIndex { get { return instance._WaveIndex; } private set { instance._WaveIndex = value; } }
    public static int WaveCount { get { return instance.myLevel.Waves.Length; } }
    public static bool WaveIsSpawning { get; private set; }

    public static bool ToggleConstantSpawning()
    {
        instance.constantSpawning = !instance.constantSpawning;
        if (instance.constantSpawning)
            NextWave();
        return instance.constantSpawning;
    }
    public static bool NextWave()
    {
        if (!GameManager.EnemiesExist && !WaveIsSpawning)
        {
            WaveIsSpawning = true;
            if (OnWaveSpawnBegun != null)
                OnWaveSpawnBegun(instance.myLevel.Waves[WaveIndex]);
            instance.StartCoroutine(instance.SpawnWave(instance.myLevel.Waves[WaveIndex++]));
            UIManager.OpenNewEventMessage(UIEventMessage.EventMessageType.WaveStarted);
            return true;
        }
        return false;
    }

    private IEnumerator SpawnWave(Wave wave)
    {
        for (int groupCount = 0; groupCount < wave.Groups.Length; groupCount++)
        {
            float spawnDelay = wave.Groups[groupCount].IntervalOfSpawns;
            float delayAfterGroup = wave.Groups[groupCount].DelayAfterGroup;
            int secondarySpawnCount = 0;
            Debug.Log(wave.Groups[groupCount].SecondarySpawnCount);
            for (int spawnCount = 0; spawnCount < wave.Groups[groupCount].MainSpawnCount;)
            {
                spawnDelay -= Time.deltaTime;
                if (GameManager.GameIsActive && spawnDelay <= 0)
                {
                    Debug.Log(secondarySpawnCount);
                    if (finalEnemies != null && finalEnemies.Length > groupCount && finalEnemies[groupCount].MainSpawnCount > spawnCount)
                        Instantiate(finalEnemies[groupCount].MainEnemyPrefab, transform.position, transform.rotation);
                    Instantiate(wave.Groups[groupCount].MainEnemyPrefab, transform.position, transform.rotation);
                    if (new System.Random().Next(0, 11) <= 6 && secondarySpawnCount++ < wave.Groups[groupCount].SecondarySpawnCount)
                        Instantiate(wave.Groups[groupCount].SecondaryEnemyPrefab, transform.position, transform.rotation);
                    spawnDelay = wave.Groups[groupCount].IntervalOfSpawns;
                    spawnCount++;
                }
                yield return new WaitForSeconds(0);
            }
            while (delayAfterGroup > 0)
            {
                delayAfterGroup -= Time.deltaTime;
                yield return new WaitForSeconds(0);
            }
        }
        WaveIsSpawning = false;
        if (OnWaveSpawningEnded != null)
            OnWaveSpawningEnded(wave);
        while (GameManager.EnemiesExist)
        {
            yield return new WaitForSeconds(0);
        }
        WaveCleared(wave);
    }
    private void WaveCleared(Wave wave)
    {
        UIManager.OpenNewEventMessage(UIEventMessage.EventMessageType.WaveCleared);
        if (OnWaveCleared != null)
            OnWaveCleared(wave);
        if (WaveIndex == WaveCount)
            GameManager.EndGame(GameResult.Result.Win);
        else if (constantSpawning)
            NextWave();
    }
}



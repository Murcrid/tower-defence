﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPanel : MonoBehaviour
{
	protected virtual void Start ()
    {
        transform.SetParent(GameObject.Find("MainPanel").transform, false);
    }
}

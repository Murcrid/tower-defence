﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class GameFile
{
    public World[] Worlds;
    [HideInInspector]
    public string PlayerName;
    [HideInInspector]
    public SaveFile.Difficulty GameDifficulty;
}
[Serializable]
public class World
{
    [HideInInspector]
    public bool IsUnlocked;
    public string Name;
    public int WorldIndex;
    public Level[] Levels;
}
[Serializable]
public class Level
{
    [HideInInspector]
    public LevelData levelData;
    public bool IsLastLevel { get { return LevelIndex == 5; } }
    public Sprite availableSprite, unavailableSprite;
    public int StartingLives = 10;
    public int StartingGold = 150;
    public int LevelIndex;
    public int WorldIndex;
    public string Name;
    public Wave[] Waves;
}

[Serializable]
public class SaveFile
{
    public enum Difficulty { Easy, Normal, Hard }
    public Difficulty GameDifficulty { get; private set; }
    public string PlayerName { get; private set; }
    public WorldData[] WorldsData = new WorldData[3];
    public SaveFile(string playerName, Difficulty gameDifficulty, bool tutorial)
    {
        bool cheat = false;
        PlayerName = playerName;
        if (PlayerName == "Murcrid")
            cheat = true;
        GameDifficulty = gameDifficulty;
        for (int i = 0; i < WorldsData.Length; i++)
        {
            if (tutorial)
                WorldsData[i] = new WorldData(i == 0, cheat);
            else
                WorldsData[i] = new WorldData(i == 1 || i == 0, cheat);
        }   
    }
}
[Serializable]
public class WorldData
{
    public LevelData[] LevelsData = new LevelData[6];
    public bool IsUnlocked;
    public WorldData(bool isUnlocked, bool cheat)
    {
        IsUnlocked = cheat ? cheat : isUnlocked;
        for (int i = 0; i < LevelsData.Length; i++)
            LevelsData[i] = new LevelData(i == 0 && IsUnlocked, cheat);
    }
}
[Serializable]
public class LevelData
{
    private bool escapedEnemiesInitialized;
    private List<EnemyGroup> _EscapedEnemies = new List<EnemyGroup>();
    public int DifferentEscapedEnemies
    {
        get
        {
            return _EscapedEnemies.Count;
        }
    }
    public int TotalEscapedEnemies
    {
        get
        {
            int numberOfEnemies = 0;
            foreach (var item in _EscapedEnemies)
            {
                numberOfEnemies += item.MainSpawnCount;
            }
            return numberOfEnemies;
        }
    }
    public List<EnemyGroup> EscapedEnemies
    {
        get
        {
            return _EscapedEnemies;
        }
        set
        {
            int numberOfNewEnemies = 0;
            foreach (var item in value)
            {
                numberOfNewEnemies += item.MainSpawnCount;
            }
            if (numberOfNewEnemies < TotalEscapedEnemies || !escapedEnemiesInitialized)
            {
                escapedEnemiesInitialized = true;
                _EscapedEnemies = value;
            }
        }
    }

    public int DifferentEscapedEnemiesCount { get; internal set; }

    public bool IsUnlocked;

    public LevelData(bool isUnlocked, bool cheat)
    {
        IsUnlocked = cheat ? cheat : isUnlocked;
    }
}

public class GameResult
{
    public enum Result { Win, Lose, Quit }
    public Result result;
    private List<EnemyGroup> _EscapedEnemies = new List<EnemyGroup>();

    public List<EnemyGroup> EscapedEnemies { get { return _EscapedEnemies; } }

    public void AddEnemyGroup(EnemyGroup group)
    {
        foreach (var item in _EscapedEnemies)
        {
            if (item.MainEnemyType == group.MainEnemyType)
            {
                item.AddSpawns(group.MainSpawnCount);
                return;
            }
        }
        _EscapedEnemies.Add(group);
    }
}

public class ToolTipInfo
{
    public readonly String name, description, infoOne, infoTwo, infoThree, infoFour;

    public ToolTipInfo(string name, string description)
    {
        this.name = name;
        this.description = description;
        infoOne = null;
    }
    public ToolTipInfo(string name, string description, string infoOne)
    {
        this.name = name;
        this.description = description;
        this.infoOne = infoOne;
    }
    public ToolTipInfo(string name, string description, string infoOne, string infoTwo)
    {
        this.name = name;
        this.description = description;
        this.infoOne = infoOne;
        this.infoTwo = infoTwo;
    }
    public ToolTipInfo(string name, string description, string infoOne, string infoTwo, string infoThree)
    {
        this.name = name;
        this.description = description;
        this.infoOne = infoOne;
        this.infoTwo = infoTwo;
        this.infoThree = infoThree;
    }
    public ToolTipInfo(string name, string description, string infoOne, string infoTwo, string infoThree, string infoFour)
    {
        this.name = name;
        this.description = description;
        this.infoOne = infoOne;
        this.infoTwo = infoTwo;
        this.infoThree = infoThree;
        this.infoFour = infoFour;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
        }            
        else if (instance != this)
            Destroy(this);
    }
    static CursorManager instance;

    [SerializeField]
    private Texture2D defaultCursor = null, selectCursor = null, buildCursor = null, errorCursor = null, targettingCursor = null;

    public enum CursorType { Default, Select, Build, Error, Targetting }
    public static CursorType CurrentCursor { get; private set; }

    private static readonly Vector2 leftCornerOffset = new Vector2(10, 10);
    private static readonly Vector2 centerOffset = new Vector2(515, 512);

    public static void ChangeCursor(CursorType mode)
    {
        if (CurrentCursor != mode)
        {
            CurrentCursor = mode;
            switch (mode)
            {
                case CursorType.Default:
                    Cursor.SetCursor(instance.defaultCursor, leftCornerOffset, CursorMode.Auto);
                    break;
                case CursorType.Select:
                    Cursor.SetCursor(instance.selectCursor, leftCornerOffset, CursorMode.Auto);
                    break;
                case CursorType.Build:
                    Cursor.SetCursor(instance.buildCursor, leftCornerOffset, CursorMode.Auto);
                    break;
                case CursorType.Error:
                    Cursor.SetCursor(instance.errorCursor, leftCornerOffset, CursorMode.Auto);
                    break;
                case CursorType.Targetting:
                    Cursor.SetCursor(instance.targettingCursor, centerOffset, CursorMode.Auto);
                    break;
            }
        }
    }
}

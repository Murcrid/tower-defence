﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Terrain))]
public class PathCreator : MonoBehaviour
{   
    public GameObject tempNodePrefab;
    public GameObject pathNodePrefab;
    public Transform pathTransform;
    public float distanceBetweenNodes = 1;
    public int smoothingValue = 5, smoothingTimes = 4;
    public int terrainWidth = 200;
    public bool endEarly = false;

    private Terrain data;
    private Vector3 nodeOnePosition;
    private Vector3 nodeTwoPosition;
    private Vector3 positionOfNextNode;

    List<GameObject> tempNodes = new List<GameObject>();

    void Start()
    {
        data = GetComponent<Terrain>();
        StartCoroutine(Draw());
    }

    private IEnumerator Draw()
    {
        Vector3 directionOfNextNode = Vector3.zero;
        while (nodeOnePosition.x <= terrainWidth && !endEarly)
        {
            if (nodeOnePosition != Vector3.zero && nodeTwoPosition != Vector3.zero)
            {
                directionOfNextNode = (nodeOnePosition - nodeTwoPosition).normalized;
                positionOfNextNode = nodeOnePosition + directionOfNextNode * distanceBetweenNodes;
                GameObject newNode = Instantiate(tempNodePrefab, positionOfNextNode, tempNodePrefab.transform.rotation);
                tempNodes.Add(newNode);
                yield return new WaitForSeconds(0.333f);
                nodeTwoPosition = nodeOnePosition;
                nodeOnePosition = newNode.transform.position;
            }
            else
            {
                bool foundHole = false;
                Vector3 beginSpot = Vector3.zero;
                Vector3 endSpot = Vector3.zero;
                for (int i = 0; i < 2; i++)
                {
                    for (int x = 0; x < 200; x++)
                    {
                        if (data.SampleHeight(new Vector3(i, 0, x)) == 0 && !foundHole)
                        {
                            foundHole = true;
                            beginSpot = new Vector3(i, 0, x);
                        }
                        else if (data.SampleHeight(new Vector3(i, 0, x)) != 0 && foundHole)
                        {
                            foundHole = false;
                            endSpot = new Vector3(i, 0, x);
                            if (Vector3.Distance(endSpot, beginSpot) <= 25)
                            {
                                GameObject node = Instantiate(pathNodePrefab, new Vector3((beginSpot.x + endSpot.x) / 2, 0, (beginSpot.z + endSpot.z) / 2), pathNodePrefab.transform.rotation);
                                node.transform.SetParent(pathTransform);
                                nodeTwoPosition = nodeOnePosition;
                                nodeOnePosition = node.transform.position;
                                break;
                            }
                        }
                    }
                    yield return new WaitForSeconds(0.05f);
                }
                if (nodeOnePosition == Vector3.zero || nodeTwoPosition == Vector3.zero)
                {
                    Debug.LogError("Couldnt find starting postions in terrain");
                    break; 
                }
            }
        }
        for (int o = 0; o < smoothingTimes; o++)
        {
            for (int i = 0; i < tempNodes.Count - 5; i++)
            {
                List<GameObject> nodes = new List<GameObject>();
                for (int x = 0; x < smoothingValue; x++)
                {
                    if (tempNodes.Count > x + i)
                    {
                        nodes.Add(tempNodes[x + i]);
                    }                        
                }
                foreach (var item in nodes)
                {
                    Vector3 test1 = nodes[nodes.Count - 1].transform.position - nodes[0].transform.position;
                    Vector3 test2 = item.transform.position - nodes[0].transform.position;
                    item.transform.position = Vector3.Project(test2, test1) + nodes[0].transform.position;
                }
                yield return new WaitForSeconds(0.02f);
            }
        }
        for (int i = 0; i < tempNodes.Count; i++)
        {
            GameObject node = Instantiate(pathNodePrefab, tempNodes[i].transform.position, pathNodePrefab.transform.rotation);
            node.transform.SetParent(pathTransform);
            Destroy(tempNodes[i]);
            yield return new WaitForSeconds(0.02f);
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempNode : MonoBehaviour {

    public Collider topCollider, botCollider, rightCollider, leftCollider;
    public Collider topleftCollider, leftbotCollider, botrightCollider, righttopCollider;

    public bool stop;

    private void Start()
    {
        Collider[] cols = GetComponentsInChildren<Collider>();
        topCollider = cols[0];
        botCollider = cols[1];
        rightCollider = cols[2];
        leftCollider = cols[3];
        topleftCollider = cols[4];
        botrightCollider = cols[5];
        righttopCollider = cols[6];
        leftbotCollider = cols[7];
        StartCoroutine(Stop());
    }
    public IEnumerator Stop()
    {
        yield return new WaitForSeconds(0.333f);
        foreach (var item in GetComponentsInChildren<Collider>())
        {
            item.enabled = false;
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag != "Terrain")
            return;
        foreach (var item in collision.contacts)
        {
            if (item.thisCollider == topCollider || item.thisCollider == leftbotCollider || item.thisCollider == botrightCollider)
            {
                transform.Translate(Vector3.back * Time.deltaTime * 4);
                break;
            }
            if (item.thisCollider == botCollider || item.thisCollider == righttopCollider || item.thisCollider == topleftCollider)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * 4);
                break;
            }
            if (item.thisCollider == leftCollider || item.thisCollider == botrightCollider || item.thisCollider == righttopCollider)
            {
                transform.Translate(Vector3.right * Time.deltaTime * 4);
                break;
            }
            if (item.thisCollider == rightCollider || item.thisCollider == leftbotCollider || item.thisCollider == topleftCollider)
            {
                transform.Translate(Vector3.left * Time.deltaTime * 4);
                break;
            }
        }
    }
}

# Tower Defence
A simple android game of a classic Tower defence. 

![Image](ScreenShots/UnityGame.png)

## Requirements
    - Unity3D 2018
       -> Download latest version of Unity from Unity.com

## Installing
1. Download repo
2. Open Lobby from tower-defence/Assets/Levels/Lobby with Unity3D

### Build with
    Unity3D
    C#

### Authors
Joni Orrensalo

![Image](ScreenShots/UnityGame2.png)